#!/usr/bin/env nextflow

process jacquard_merge {
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(bam) - BAM File
//  path(bed) - BED File
//
// output:
//   tuple => emit: filt_bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*filt.bam")

// require:
//   VCFS

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/jacquard_merge"
  label 'jacquard_container'
  label 'jacquard_merge'
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(vcfs)
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*union.vcf"), emit: union_vcfs

  script:
  """
  # This is a cheap hack to apply heterozygous genotypes to Strelka2 somatic
  # variants. This should be in its own somatic-specific process.
  mkdir unused
  mv ${dataset}*strelka2* unused

  zcat unused/*strelka2*indels*gz | sed -n '/^chr/{s/\\t/:GT\\t/9};p' | sed -n '/^chr/{s/\\t/:0\\/0\\t/10};p' | sed -n '/^chr/{s/\$/:0\\/1/};p' | gzip > ${dataset}-${pat_name}-${norm_run}_${tumor_run}.strelka2.indels.added_gt.vcf.gz
  zcat unused/*strelka2*snvs*gz | sed -n '/^chr/{s/\\t/:GT\\t/9};p' | sed -n '/^chr/{s/\\t/:0\\/0\\t/10};p' | sed -n '/^chr/{s/\$/:0\\/1/};p' | gzip > ${dataset}-${pat_name}-${norm_run}_${tumor_run}.strelka2.snvs.added_gt.vcf.gz



  mkdir -p unzipped_vcfs
  for i in *vcf.gz; do
    cp \${i} unzipped_vcfs
  done

  cd unzipped_vcfs
  gunzip *
  for i in *vcf; do
    sed -i 's/	NORMAL/	${dataset}-${pat_name}-${norm_run}/g' \${i}
    sed -i 's/	TUMOR/	${dataset}-${pat_name}-${tumor_run}/g' \${i}
  done
  cd ..

  jacquard merge unzipped_vcfs ${dataset}-${pat_name}-${norm_run}_${tumor_run}.union.vcf --include_rows=all ${parstr}

  sed -i 's/${dataset}-${pat_name}-${norm_run}_${tumor_run}|//g' ${dataset}-${pat_name}-${norm_run}_${tumor_run}.union.vcf

  rm -rf unzipped_vcfs
  rm -rf tmp
  """
}

process jacquard_merge_tumor {
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(bam) - BAM File
//  path(bed) - BED File
//
// output:
//   tuple => emit: filt_bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*filt.bam")

// require:
//   VCFS

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/jacquard_merge"
  label 'jacquard_container'
  label 'jacquard_merge'
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(vcfs)
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*concatd.vcf"), emit: concatd_vcfs

  script:
  """
  mkdir -p unused
  cp ${dataset}-${pat_name}-${norm_run}*deepv* ${dataset}-${pat_name}-${norm_run}_${tumor_run}.germ.vcf.gz
  mv ${dataset}-${pat_name}-${norm_run}*deepv* unused

  mkdir -p unzipped_vcfs
  for i in *vcf.gz; do
    cp \${i} unzipped_vcfs
  done

  cd unzipped_vcfs
  gunzip *
  cd ..

  jacquard merge unzipped_vcfs ${dataset}-${pat_name}-${norm_run}_${tumor_run}.concatd.vcf ${parstr}

  rm -rf unzipped_vcfs
  rm -rf tmp
  """
}
